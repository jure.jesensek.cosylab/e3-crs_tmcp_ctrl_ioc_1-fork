# require statments should only specify the module name
# The version of the module shall be defined in the environment.yaml file
# Note that all modules shall be in lowercase
## Add extra modules here
require s7plc

# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables


## The CRYO Discipline should be changed to Cryo !!!!!!!!!!!!!!!!!!!!!!
epicsEnvSet("PREFIX", "CrS-TMCP:CRYO-")

epicsEnvSet("LOCKTIME", "300")

#IP of PLC
epicsEnvSet("IPADDR", "172.16.114.42")
#epicsEnvSet("DEVICE", "EDIT: change this")
#epicsEnvSet("LOCATION", "EDIT: change this")
#epicsEnvSet("ENGINEER", "EDIT: Name <email@esss.se>"
## Add extra environment variables here

# Load standard module startup scripts
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

## Add extra startup scripts requirements here
# iocshLoad("$(module_DIR)/module.iocsh", "MACRO=MACRO_VALUE")

## Load custom databases
cd $(E3_IOCSH_TOP)

######### PLC to IOC
dbLoadRecords("db/header_sysPLCtoIOC.db", "PREFIX=${PREFIX}, PLC_NAME=DB1101")
dbLoadRecords("db/DB1100.db", "PREFIX=${PREFIX}, PLC_NAME=DB1100")
dbLoadRecords("db/DB1102.db", "PREFIX=${PREFIX}, PLC_NAME=DB1102")
dbLoadRecords("db/DB1103.db", "PREFIX=${PREFIX}, PLC_NAME=DB1103")
dbLoadRecords("db/DB1104.db", "PREFIX=${PREFIX}, PLC_NAME=DB1104")
dbLoadRecords("db/DB1106.db", "PREFIX=${PREFIX}, PLC_NAME=DB1106")
dbLoadRecords("db/DB1107.db", "PREFIX=${PREFIX}, PLC_NAME=DB1107")
dbLoadRecords("db/DB1108.db", "PREFIX=${PREFIX}, PLC_NAME=DB1108")
dbLoadRecords("db/DB1112.db", "PREFIX=${PREFIX}, PLC_NAME=DB1112")
dbLoadRecords("db/DB1113.db", "PREFIX=${PREFIX}, PLC_NAME=DB1113")
dbLoadRecords("db/DB1114.db", "PREFIX=${PREFIX}, PLC_NAME=DB1114")
dbLoadRecords("db/DB1116.db", "PREFIX=${PREFIX}, PLC_NAME=DB1116")
dbLoadRecords("db/DB1118.db", "PREFIX=${PREFIX}, PLC_NAME=DB1118")


######### IOC to PLC
dbLoadRecords("db/header_sysIOCtoPLC.db", "PREFIX=${PREFIX}, PLC_NAME=DB1133, PORT=2012, BYTES=20")
dbLoadRecords("db/DB1132.db", "PREFIX=${PREFIX}, PLC_NAME=DB1132")
dbLoadRecords("db/DB1134.db", "PREFIX=${PREFIX}, PLC_NAME=DB1134")
dbLoadRecords("db/DB1136.db", "PREFIX=${PREFIX}, PLC_NAME=DB1136")
dbLoadRecords("db/DB1142.db", "PREFIX=${PREFIX}, PLC_NAME=DB1142")
dbLoadRecords("db/DB1144.db", "PREFIX=${PREFIX}, PLC_NAME=DB1144")
dbLoadRecords("db/DB1146.db", "PREFIX=${PREFIX}, PLC_NAME=DB1146")
dbLoadRecords("db/DB1148.db", "PREFIX=${PREFIX}, PLC_NAME=DB1148")


######### Man Auto
dbLoadRecords("db/manAuto.db", "PREFIX=${PREFIX}")


######### Alarm line
dbLoadRecords("db/DB1100alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1102alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1103alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1104alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1107alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1108alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1112alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1113alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1114alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1116alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/DB1118alarms.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/last_alarm.db", "PREFIX=${PREFIX}")


######### Load to PLC
dbLoadRecords("db/load_to_plc.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/direct_load.db", "PREFIX=${PREFIX}")


######### Restore from PLC
dbLoadRecords("db/restore_from_plc.db", "PREFIX=${PREFIX}")
dbLoadRecords("db/rb_to_sp.db", "PREFIX=${PREFIX}")

######### SCREENLOCK
dbLoadRecords("db/ScreenLock.db", "PREFIX=${PREFIX}, LOCKTIME=$(LOCKTIME), MAINID=CR")
dbLoadRecords("db/ScreenLockLocal.db", "PREFIX=${PREFIX}, LOCKTIME=$(LOCKTIME), LOCKID=ONE")
dbLoadRecords("db/ScreenLockLocal.db", "PREFIX=${PREFIX}, LOCKTIME=$(LOCKTIME), LOCKID=TWO")
dbLoadRecords("db/ScreenLockLocal.db", "PREFIX=${PREFIX}, LOCKTIME=$(LOCKTIME), LOCKID=LCR1")

######### Configure s7plc
#s7plcConfigure (PLCname, IPaddr, port, inSize[B], outSize[B], bigEndian, recvTimeout[ms], sendInterval[ms])

s7plcConfigure("DB1100", "${IPADDR}", 2030, 1776, 0, 1,  1000,   100)
s7plcConfigure("DB1101", "${IPADDR}", 2011,   20, 0, 1,  1000,   100)
s7plcConfigure("DB1102", "${IPADDR}", 2032, 1420, 0, 1,  1000,   100)
s7plcConfigure("DB1103", "${IPADDR}", 2002,   44, 0, 1,  1000,   100)
s7plcConfigure("DB1104", "${IPADDR}", 2003,  532, 0, 1,  1000,   100)
s7plcConfigure("DB1106", "${IPADDR}", 2004, 1700, 0, 1,  5000,  1000)
s7plcConfigure("DB1107", "${IPADDR}", 2007, 1100, 0, 1,  5000,  1000)
s7plcConfigure("DB1108", "${IPADDR}", 2017,  640, 0, 1,  1000,   100)
s7plcConfigure("DB1112", "${IPADDR}", 2019,  140, 0, 1,  1000,   100)
s7plcConfigure("DB1113", "${IPADDR}", 2001,   34, 0, 1,  1000,   100)
s7plcConfigure("DB1114", "${IPADDR}", 2008,  864, 0, 1,  5000,  1000)
s7plcConfigure("DB1116", "${IPADDR}", 2013,  560, 0, 1,  5000,  1000)
s7plcConfigure("DB1118", "${IPADDR}", 2014,  426, 0, 1,  5000,  1000)

s7plcConfigure("DB1132", "${IPADDR}", 2005, 0, 1020, 1,  1000,   100)
s7plcConfigure("DB1133", "${IPADDR}", 2012, 0,   20, 1,  1000,   100)
s7plcConfigure("DB1134", "${IPADDR}", 2006, 0,   84, 1,  1000,   100)
s7plcConfigure("DB1136", "${IPADDR}", 2009, 0, 1700, 1,  5000,  1000)
s7plcConfigure("DB1142", "${IPADDR}", 2021, 0,  100, 1,  1000,   100)
s7plcConfigure("DB1144", "${IPADDR}", 2010, 0,  256, 1,  5000,  1000)
s7plcConfigure("DB1146", "${IPADDR}", 2015, 0,  364, 1,  5000,  1000)
s7plcConfigure("DB1148", "${IPADDR}", 2016, 0,  256, 1,  5000,  1000)
# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here

